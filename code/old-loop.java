for (t : T) {
  affectedFiles = getFilesAffectedByTest(t)
  affectsThisTest = false
  if (affectedFiles.empty()) {
    affectsThisTest = true
  } else {
    for (f : affectedFiles) {
      currentFile = F.findFile(f)
      if (f.state != currentFile.state) {
        affectsThisTest = true
        break
  } } }
  if (affectsThisTest) {
    result.add(t)
} }
