---?image=traefik.png&position=50% 15%&size=auto 60%

@title[intro]
@snap[south]
@size[1.5em](Änderungsbasierte Testfallreduktion)
@snapend

---

@title[Agenda]
## Agenda

- Was ist Testfallreduktion?
- Warum Testfallreduktion?
- Existierende Verfahrensarten
- Clover als Programm aus der Praxis

---

## Was ist Testfallreduktion?

+++

![testfaelle](images/testfaelle.png)
## Was ist Testfallreduktion?

+++

![testfaelle](images/testfaelle-ausgegraut.png)
## Was ist Testfallreduktion?

---

## Warum Testfallreduktion?

+++

![lange-laufzeiten](images/warum-tfr.png)
## Warum Testfallreduktion?

+++

![lange-laufzeiten](images/warum-tfr-markiert.png)
## Warum Testfallreduktion?

---

## Ziel: Dauer der Tests automatisiert reduzieren

---

![literatur](images/literatur.png)

---

## Existierende Verfahrensarten

@ul
- Evolutionär
- Gierig (Greedy)
- Hybrid
@ulend

---

## Evolutionäre Algorithmen

---

![vererbung](images/genetischer-algorithmus.png)

---

## Gierige Algorithmen

Note:

Großes Problem, welches so umgewandelt wird, dass es aus kleineren Problemen besteht.
Optimales Lösen der kl. Probleme löst großes Problem.

+++

![grosse-entscheidung](images/grosse-entscheidung.png)

+++

![kleine-entscheidungen](images/kleine-entscheidungen.png)

---

![nichts-gefunden](images/nichts-passendes.png)

---

![selbst-entwickeln?](images/selbst-entwickeln.png)

---

![clover-gefunden](images/clover.png)

+++

![clover-report](images/2019-02-18-Clover-Report.png)

Note:

Auf den ersten Blick nur Testabdeckung als Funktionalität ersichtlich, wie bei anderen Programmen

+++

![clover-coverage](images/2019-02-18-CoveragePerPackageAndFile.png)

Note:

Andere Ansicht, Integration in IDE

+++

![optimized-tests](images/optimized-tests.png)

Note:

Optimiert ja doch

---

![optimierung](images/optimierung.png)

+++?code=code/old-loop.java&lang=java&title=Vorhandenes Optimierungsverfahren

+++

![git](images/git-logo.png)

+++

getFile2TestsMap()

Note:

Erste Idee: Diese Methode benutzen, um direkt die Tests hinzuzufügen.
Verbindung mit generischem Code nicht sinnvoll möglich gewesen.

+++?code=code/get-tests-by-diff.java&lang=java

+++

![changed-tests](images/changed-tests.png)

+++

![filev1](images/testv1.png)

Note:

Tests, die unverändert blieben, aber deren abgedeckte Dateien verändert wurden, wurden nicht mehr ausgeführt.

+++

![filev2](images/testv2.png)

+++?code=code/has-changes.java&lang=java

Note:

Neuer Ansatz: Clover-Datenbank nach abgedeckten Dateien abfragen, und direkt über geänderte Dateien disjunkt-Eigenschaft ermitteln

+++

![changed-files](images/changed-files.png)

---

![ergebnis](images/fazit.png)

+++

![testergebnisse](images/test-results.png)

+++

## Optimierung möglich

+++

## Overhead groß

---

